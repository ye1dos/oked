import React,{useState} from 'react'
import {Table, TableRow, TableBody, TableCell, TablePagination, TableHead, Paper} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import SimpleModal from './Modal'
import Form from './Form'
import AddCircleIcon from '@material-ui/icons/AddCircle';
const columns = [
    {
        id: 'region',
        label: 'Region',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
      },
      {
        id: 'oked',
        label: 'Oked',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
      },
      {
        id: 'period',
        label: 'Period',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toFixed(2),
      },
      {
        id: 'zna4',
        label: 'Zna4eniye',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
      },
      {
        id: 'edit',
        label: 'Edit',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
      },
      {
        id: 'del',
        label: 'Delete',
        minWidth: 170,
        align: 'right',
        format: (value) => value.toLocaleString('en-US'),
      }
  ]

const useStyles = makeStyles({
    root: {
      width: '100%',
      paddingBottom: 20
    },
    container: {
      maxHeight: 440,
    },
});

const TableComponent = ({data}) => {
  const [rows, setRows] = useState(data)
    const classes = useStyles();
    const [page, setPage] = useState(0);
    const [editid, setEditId] = useState(-1)
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
      };
    const [form, setForm] = useState(false)
// Modal
  const [open, setOpen] = useState();
  const [elem, setElem] = useState(null)
  const openForm = () => {
    setForm(true)
  }
  const handleClose = () => {
    setOpen(false);
    setElem(null);
  };
  const handleCloseForm = () => {
    setForm(false);
  };
      const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
      };
      // Edit
      const handleEdit = (i) => {
        setOpen(true);
        setElem(rows[i])
        setEditId(i)
      }
      const takeOne = (child) => {
        let arr = [...rows]
        arr[editid] = child
        setRows(arr)
      }
      const handleDelete = (i) => {
        // console.log("Handled")
        setRows(rows.filter((el, j) => j !== i))
        console.log(rows)
      }
      const takeParams = (child) => {
        setRows([...rows, child])
      }
    return (
      <Paper className={classes.root}>
        <Table stickyHeader aria-label="sticky table">
        <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell>
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((el, i) => (
                <TableRow hover role="checkbox" tabIndex={-1}>
                        <TableCell>{el.Region}</TableCell>
                        <TableCell>{el.Oked}</TableCell>
                        <TableCell>{el.Period}</TableCell>
                        <TableCell>{el.Value}</TableCell>
                        <TableCell><EditIcon onClick={() => handleEdit(i)}/></TableCell>
                        <TableCell><DeleteForeverIcon onClick={() => handleDelete(i)}/></TableCell>
                </TableRow>
                ))}
            </TableBody>
        </Table>
        <TablePagination
        rowsPerPageOptions={[5, 10, 20]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      {open ? <SimpleModal
          open={open}
          handleClose={handleClose}
          aria-labelledby="title"
          aria-describedby="description"
          elem={elem}
          takeParams={takeOne}
        /> : null}
        <AddCircleIcon fontSize="large" onClick={() => openForm()}/>
        {form ? <Form 
          open={form}
          takeParams={takeParams}
          handleClose={handleCloseForm}/> : null}
      </Paper>
    );
}
export default TableComponent;