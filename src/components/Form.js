import React, {useState} from "react";
import TextField from '@material-ui/core/TextField';
import {Button} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import {oked} from '../static/oked'
import {region} from '../static/region'
import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { Input } from '@material-ui/core';
import {DatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment'

function getModalStyle() {
  const top = 50
  const left = 50

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 500,
    height: 300,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    padding: theme.spacing(2, 4, 3),
    minWidth: 120,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const Form = ({open, handleClose, takeParams}) => {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const [state, setState] = useState(
    {
    Region: "",
    Oked: "",
    Period: "20.02.2021",
    Value: ""
})
  const [date, setDate] = useState(new Date())

  const change = e => {
    e.preventDefault();
    setState({...state, [e.target.name]: e.target.value
    });
  };


  const onSubmit = e => {
    e.preventDefault();
    console.log(date)
    takeParams(state)
      setState({
        Region: "",
        Oked: "",
        Period: "",
        Value: ""
      });
      handleClose()
  };
  const handleDateChange = (date) => {
    setDate(date)
    setState({...state, Period: moment(date).format('DD.MM.YYYY')})
  }

    return (
      <Modal open={open} onClose={handleClose}>
        <div style={modalStyle} className={classes.paper}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <div>
        <InputLabel id="demo-simple-select-label">Region</InputLabel>
          <Select
          style={{ width: 300}}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          name="Region"
          onChange = {(e) => setState({...state, Region: e.target.value})}
        >
        {region.map((el, i) => {
              return (<MenuItem value={el.Name}>{el.Name}</MenuItem>)
            })}
        </Select>
        </div>
          <Autocomplete
            id="combo-box-demo"
            options={oked}
            name="Oked"
            getOptionLabel={(option) => option.Name}
            style={{ width: 300}}
            onInputChange={(e)=> setState({...state, Oked: e.target.textContent})}
            renderInput={(params) => (
              <TextField {...params} label="Oked" variant="outlined" />
            )}
          />
          <DatePicker
            name="Period"
            value={date}
            format="dd.MM.yyyy"
            placeholder="Period"
            style={{ width: 300}}
            onChange={(date) => handleDateChange(date)}
            
            />
            <br/>
          <Input name="Value" placeholder="Value" type="number" style={{ width: 300}} onChange={(e) => change(e)} />
          {console.log(state)}
          <br/>
          <Button style={{ width: 300}} onClick={(e) => onSubmit(e)}>Submit</Button>
          </MuiPickersUtilsProvider>
          </div>
      </Modal>
    );
}

export default Form;