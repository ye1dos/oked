import './App.css';
import data from './static/newdata'
import TableComponent from './components/Table';
function App() {
  return (
    <div className="App">
      <TableComponent data={data}/>
      {console.log(data)}
    </div>
  );
}

export default App;