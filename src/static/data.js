export const example = [{ 
    "Region": "710000000",
    "Oked": "13",
    "Period": "31.12.2020",
    "Value": 100.0
    },
    { "Region": "710000000",
    "Oked": "13",
    "Period": "31.12.2021",
    "Value": 98.9
    },
    { "Region": "710000000",
    "Oked": "3",
    "Period": "31.12.2020",
    "Value": 99.0
    },
    { "Region": "710000000",
    "Oked": "3",
    "Period": "31.12.2021",
    "Value": 96.9
    },
    { "Region": "710000000",
    "Oked": "16",
    "Period": "31.12.2020",
    "Value": 99.6
    },
    { "Region": "710000000",
    "Oked": "16",
    "Period": "31.12.2021",
    "Value": 97.8
    },
    { "Region": "750000000",
    "Oked": "13",
    "Period": "31.12.2020",
    "Value": 95.0
    },
    { "Region": "750000000",
    "Oked": "13",
    "Period": "31.12.2021",
    "Value": 97.9
    },
    { "Region": "750000000",
    "Oked": "3",
    "Period": "31.12.2020",
    "Value": 97.6
    },
    { "Region": "750000000",
    "Oked": "3",
    "Period": "31.12.2021",
    "Value": 96.9
    },
    { "Region": "750000000",
    "Oked": "16",
    "Period": "31.12.2020",
    "Value": 99.6
    },
    { "Region": "750000000",
    "Oked": "16",
    "Period": "31.12.2021",
    "Value": 94.3
    },
    { 
    "Region": "790000000",
    "Oked": "13",
    "Period": "31.12.2020",
    "Value": 92.0
    },
    { "Region": "790000000",
    "Oked": "13",
    "Period": "31.12.2021",
    "Value": 94.0
    },
    { "Region": "790000000",
    "Oked": "3",
    "Period": "31.12.2020",
    "Value": 98.1
    },
    { "Region": "790000000",
    "Oked": "3",
    "Period": "31.12.2021",
    "Value": 95.9
    },
    { "Region": "790000000",
    "Oked": "16",
    "Period": "31.12.2021",
    "Value": 94.3
    },
    { "Region": "790000000",
    "Oked": "8",
    "Period": "31.12.2020",
    "Value": 96.2
    },
    { "Region": "790000000",
    "Oked": "8",
    "Period": "31.12.2021",
    "Value": 99.9
    }]
    